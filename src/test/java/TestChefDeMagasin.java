import org.junit.jupiter.api.Test;
import stock_gestion.ChefDeMagasin;
import stock_gestion.Magasin;
import stock_gestion.Rayon;

import static org.junit.jupiter.api.Assertions.*;

class TestChefDeMagasin {

	private final Magasin leMag = new Magasin("CyrilHanounaLeMag");
	private final Rayon unRayon = new Rayon("Chasse&Pêche", leMag);
	private final ChefDeMagasin bob = new ChefDeMagasin("Nom", "Prenom", "prenom.nom@mail.com", leMag);

	@Test
	void createRayonTest() {
		bob.createRayon("Fromages");
		assertEquals(bob.consulterRayon("Fromages").getNom(), "Fromages");
	}

	@Test
	void modifyRayonTest() {
		bob.getMag().addRayon(unRayon);
		bob.modifierUnRayon(unRayon.getNom(), "Pêche&Chasse");
		assertNotNull(bob.consulterRayon("Pêche&Chasse"));
	}

	@Test
	void deleteRayonTest() {
		Rayon fromages = new Rayon("Fromages", leMag);
		bob.removeRayon(fromages);
		assertNull(bob.consulterRayon("Fromages"));
	}
}
