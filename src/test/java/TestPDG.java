import org.junit.jupiter.api.Test;
import stock_gestion.Magasin;
import stock_gestion.PDG;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TestPDG {

	private final Magasin leMag = new Magasin("CyrilHanounaLeMag");
	private ArrayList<Magasin> magasins = new ArrayList<>();
	private final PDG bob = new PDG("Nom", "Prenom", "prenom.nom@mail.com", magasins);

	@Test
	void createMagasinTest() {
		bob.createMagasin(new Magasin("Biathlon fois 5"));
		assertEquals(bob.consulterMagasin("Biathlon fois 5").getNom(), "Biathlon fois 5");
	}

	@Test
	void modifyMagasinTest() {
		bob.addMagasin(leMag);
		bob.modifierMagasin(leMag, "Dodécathlon moins 2");
		assertNotNull(bob.consulterMagasin("Dodécathlon moins 2"));
	}

	@Test
	void deleteMagasinTest() {
		Magasin unMag = new Magasin("Dodécathlon moins 2");
		bob.removeMagasin(unMag);
		assertNull(bob.consulterMagasin("Dodécathlon moins 2"));
	}

}
