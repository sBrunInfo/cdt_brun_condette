import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import stock_gestion.Article;
import stock_gestion.ChefDeRayon;
import stock_gestion.Magasin;
import stock_gestion.Rayon;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TestChefDeRayon {

	private Magasin leMag = new Magasin("CyrilHanounaLeMag");
	private Rayon unRayon = new Rayon("Chasse&Pêche", leMag);
	private Article banane = new Article("banane", "BD5E89F", 50, 1.50);
	private ChefDeRayon bob = new ChefDeRayon("Nom", "Prenom", "prenom.nom@mail.com", leMag, unRayon);

	@BeforeEach
	void setUp() {
		bob.getRayon().getArticles().add(banane);
	}

	@AfterEach
	void tearDown() {
		bob.getRayon().getArticles().clear();
	}

	@Test
	void getMagasinTest() {
		Magasin mag = bob.consulterMagasin();
		assertEquals(mag, leMag);
	}

	@Test
	void getRayonTest() {
		leMag.getListeRayons().add(unRayon);
		assertEquals(bob.consulterRayon(unRayon.getNom()), bob.getRayon());
	}

	@Test
	void addArticleTest() {
		bob.ajouterArticleDansSonRayon(banane);
		assertEquals(bob.getRayon().getArticles().get(bob.getRayon().getArticles().size() - 1), banane);
	}

	@Test
	void getArticleTest() {
		Article first = bob.getArticleDansSonRayon(0);
		assertEquals(first, banane);
	}

	@Test
	void createArticleTest() {
		bob.createNewArticle("patate", "BD5E65P", 50, 1.50);
		assertNotNull(bob.getArticleDansSonRayon("BD5E65P"));
	}

	@Test
	void deleteArticleTest() {
		bob.deleteArticleDansSonRayon(0);
		assertEquals(bob.getRayon().getArticles().size(), 0);
	}
}
