package stock_gestion;

import java.util.ArrayList;

public class Magasin {
	private String nom;
	private ArrayList<Rayon> listeRayons;
	private ArrayList<ChefDeRayon> chefsRayons;

	public Magasin(String nom) {
		this.nom = nom;
		this.listeRayons = new ArrayList<Rayon>();
		this.chefsRayons = new ArrayList<ChefDeRayon>();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Rayon> getListeRayons() {
		return listeRayons;
	}

	public void setListeRayons(ArrayList<Rayon> listeRayons) {
		this.listeRayons = listeRayons;
	}

	public ArrayList<ChefDeRayon> getChefsRayons() {
		return chefsRayons;
	}

	public void setChefsRayons(ArrayList<ChefDeRayon> chefsRayons) {
		this.chefsRayons = chefsRayons;
	}

	public Rayon getRayon(String nom) {
		for (Rayon listeRayon : listeRayons) {
			if (listeRayon.getNom().equals(nom)) return listeRayon;
		}
		return null;
	}

	public void addRayon(Rayon unRayon) {
		this.getListeRayons().add(unRayon);
	}

	public void addChefRayon(ChefDeRayon unChefRayon) {
		this.getChefsRayons().add(unChefRayon);
	}

	public String toString() {
		return this.nom;
	}
}
