package stock_gestion;

import java.util.ArrayList;

public class Rayon {
	private String nom;
	private ArrayList<Article> articles;
	private Magasin mag;

	public Rayon(String nom, Magasin mag) {
		this.nom = nom;
		this.articles = new ArrayList<Article>();
		this.mag = mag;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Magasin getMag() {
		return mag;
	}

	public void setMag(Magasin mag) {
		this.mag = mag;
	}

	public ArrayList<Article> getArticles() {
		return articles;
	}

	public void setArticles(ArrayList<Article> articles) {
		this.articles = articles;
	}

	public String toString() {
		return this.nom;
	}
}
