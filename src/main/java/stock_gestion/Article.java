package stock_gestion;

public class Article {
	private String nom;
	private String ref;
	private int quantite;
	private double prixUnitaire;

	public Article(String nom, String ref, int quantite, double prixUnitaire) {
		this.nom = nom;
		this.ref = ref;
		this.quantite = quantite;
		this.prixUnitaire = prixUnitaire;
	}

	public Article(String nom, String ref, double prixUnitaire) {
		this.nom = nom;
		this.ref = ref;
		this.quantite = 0;
		this.prixUnitaire = prixUnitaire;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public double getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public String toString() {
		return this.nom;
	}
}
