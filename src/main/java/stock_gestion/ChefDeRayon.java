package stock_gestion;

public class ChefDeRayon extends Personne {
	private Rayon rayon;
	private Magasin mag;

	public ChefDeRayon(String nom, String prenom, String email, Magasin mag, Rayon rayon) {
		super(nom, prenom, email);
		this.rayon = rayon;
		this.mag = mag;
		mag.addChefRayon(this);
	}

	public Rayon getRayon() {
		return rayon;
	}

	public void setRayon(Rayon rayon) {
		this.rayon = rayon;
	}

	public Magasin getMag() {
		return mag;
	}

	public void setMag(Magasin mag) {
		this.mag = mag;
	}

	public Rayon consulterRayon(String rayonName) {
		return mag.getRayon(rayonName);
	}

	public String modifierRayon(String rayonName, String newRayonName) {
		if (mag.getRayon(rayonName).equals(this.rayon))
			mag.getRayon(rayonName).setNom(newRayonName);
		return newRayonName;
	}

	public Magasin consulterMagasin() {
		return this.mag;
	}

	public void ajouterArticleDansSonRayon(Article banane) {
		this.rayon.getArticles().add(banane);
	}

	public Article getArticleDansSonRayon(int i) {
		if (this.rayon.getArticles().get(i) != null)
			return this.rayon.getArticles().get(i);
		return null;
	}

	public Article getArticleDansSonRayon(String ref) {
		for (Article article : this.rayon.getArticles()) {
			if (article.getRef().equals(ref)) return article;
		}
		return null;
	}

	public void createNewArticle(String nom, String ref, int quantite, double prix) {
		this.rayon.getArticles().add(new Article(nom, ref, quantite, prix));
	}

	public void deleteArticleDansSonRayon(int i) {
		this.rayon.getArticles().remove(i);
	}
}
