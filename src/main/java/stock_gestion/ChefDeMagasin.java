package stock_gestion;

import java.util.concurrent.atomic.AtomicBoolean;

public class ChefDeMagasin extends Personne {

	private Magasin mag;

	public ChefDeMagasin(String nom, String prenom, String email, Magasin mag) {
		super(nom, prenom, email);
		this.mag = mag;
	}

	public Magasin getMag() {
		return mag;
	}

	public void setMag(Magasin mag) {
		this.mag = mag;
	}

	public void createRayon(String nom) {
		AtomicBoolean exist = new AtomicBoolean(false);
		for (Rayon ray : this.getMag().getListeRayons()) {
			if (ray.getNom().equals(nom))
				exist.set(true);
		}
		if (!exist.get())
			this.getMag().getListeRayons().add(new Rayon(nom, this.mag));
	}

	public void removeRayon(Rayon rayon) {
		this.getMag().getListeRayons().removeIf(ray -> ray.equals(rayon));
	}

	public Rayon consulterRayon(String rayonName) {
		return mag.getRayon(rayonName);
	}

	public void modifierUnRayon(String rayonName, String newRayonName) {
		if (mag.getRayon(rayonName) != null)
			mag.getRayon(rayonName).setNom(newRayonName);
	}

	public Magasin consulterMagasin() {
		return this.mag;
	}

	public void ajouterArticleDansUnRayon(Article banane, String rayon) {
		this.mag.getRayon(rayon).getArticles().add(banane);
	}

	public Article getArticleDansUnRayon(int i, String rayon) {
		return this.mag.getRayon(rayon).getArticles().get(i);
	}

	public void createNewArticle(String nom, String ref, int quantite, double prix, String rayon) {
		this.mag.getRayon(rayon).getArticles().add(new Article(nom, ref, quantite, prix));
	}

	public void deleteArticleDansUnRayon(int i, String rayon) {
		this.mag.getRayon(rayon).getArticles().remove(i);
	}
}
