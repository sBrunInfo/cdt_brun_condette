package stock_gestion;

import java.util.ArrayList;

public class PDG extends Personne {

	private ArrayList<Magasin> mags;

	public PDG(String nom, String prenom, String email) {
		super(nom, prenom, email);
		this.mags = BDD.magasins;
	}

	public PDG(String nom, String prenom, String email, ArrayList<Magasin> mags) {
		super(nom, prenom, email);
		this.mags = mags;
	}

	public void createMagasin(Magasin magasin) {
		this.mags.add(magasin);
	}

	public Magasin consulterMagasin(String nom) {
		for (Magasin mag : this.mags) {
			if (mag.getNom().equals(nom)) return mag;
		}
		return null;
	}

	public void modifierMagasin(Magasin leMag, String nom) {
		for (Magasin mag : this.mags) {
			if (mag.equals(leMag)) mag.setNom(nom);
		}
	}

	public void removeMagasin(Magasin leMag) {
		this.mags.removeIf(mag -> mag.equals(leMag));
	}

	public ArrayList<Magasin> getMags() {
		return mags;
	}

	public void setMags(ArrayList<Magasin> mags) {
		this.mags = mags;
	}

	public void addMagasin(Magasin leMag) {
		this.mags.add(leMag);
	}
}
