package stock_gestion;

import java.util.ArrayList;

public class BDD {
	public static ArrayList<Magasin> magasins = new ArrayList<>();
	public static ArrayList<Personne> personnel = new ArrayList<>();


	public static void instantiateLists() {
		personnel.add(new PDG("Bohard", "Henri", "henri.bohard@gmail.com", magasins));
		magasins.add(new Magasin("Dodecathlon"));
		magasins.get(0).addRayon(new Rayon("Chasse & Peche", magasins.get(0)));
		ArrayList<Article> articles = new ArrayList<>();
		articles.add(new Article("Fusil de chasse", "R53F89H", 120, 150));
		articles.add(new Article("Harpon", "R28F51G", 120, 80));
		magasins.get(0).getRayon("Chasse & Peche").setArticles(articles);

		magasins.add(new Magasin("Saveurs d'autrefois"));
		magasins.get(1).addRayon(new Rayon("Viandes faisandees", magasins.get(1)));
		ArrayList<Article> articles2 = new ArrayList<>();
		articles2.add(new Article("Steak de blaireau", "R27F65K", 50, 5));
		articles2.add(new Article("Jambon de coyote", "R44F76H", 20, 20));
		magasins.get(1).getRayon("Viandes faisandees").setArticles(articles2);

		personnel.add(new ChefDeMagasin("Garond", "Ferdinand", "ferdiannd.garond@gmail.com", magasins.get(0)));
		personnel.add(new ChefDeRayon("Houflare", "Gertrude", "gertrude.houflare@gmail.com", magasins.get(0), magasins.get(0).getRayon("Chasse & Peche")));
		personnel.add(new ChefDeMagasin("Dubout", "Jacques", "jacques.dubout@gmail.com", magasins.get(1)));

		personnel.add(new ChefDeRayon("Vetre", "Nicolas", "nicolas.vetre@gmail.com", magasins.get(1), magasins.get(1).getRayon("Viandes faisandees")));
	}
}
