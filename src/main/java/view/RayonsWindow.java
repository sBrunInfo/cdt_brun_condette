package view;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import stock_gestion.BDD;
import stock_gestion.Magasin;
import stock_gestion.Rayon;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

public class RayonsWindow extends Application {

	@FXML
	private ListView<Rayon> listRayons;

	@Override
	public void start(final Stage primaryStage) {
		try {
			final URL url = getClass().getResource("/rayonsWindow.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(url);
			final AnchorPane root = fxmlLoader.load();
			final Scene scene = new Scene(root, 500, 450);
			primaryStage.setScene(scene);
		} catch (IOException ex) {
			System.err.println("Erreur au chargement: " + ex);
		}
		primaryStage.setTitle("Liste des membres du personnel");

		primaryStage.show();
	}

	public void initialize() {
		for (Magasin mag : BDD.magasins) {
			listRayons.getItems().addAll(mag.getListeRayons());
		}
	}

	public void onModifyClick() {
		TextInputDialog dialog = new TextInputDialog(listRayons.getSelectionModel().getSelectedItem().getNom());
		dialog.setTitle("Modification d'un Rayon");
		dialog.setHeaderText(null);
		dialog.setContentText("Nom:");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			listRayons.getSelectionModel().getSelectedItem()
					.getMag().getRayon(listRayons.getSelectionModel().getSelectedItem().getNom()).setNom(result.get());
			listRayons.getSelectionModel().getSelectedItem().setNom(result.get());
		}
		listRayons.refresh();
	}

	public void onDeleteClick() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Confirmation de la suppression");
		alert.setContentText("Voulez vous vraiment supprimer l'objet selectionne?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			listRayons.getSelectionModel().getSelectedItem()
					.getMag().getListeRayons().remove(listRayons.getSelectionModel().getSelectedItem());
			listRayons.getItems().remove(listRayons.getSelectionModel().getSelectedIndex());
		}
		listRayons.refresh();

	}

	public void onAddClick() {
		Dialog<ButtonType> dialog = new Dialog<>();
		dialog.setTitle("Ajout d'un rayon");
		dialog.setHeaderText(null);


		ButtonType addButtonType = new ButtonType("Ajouter", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(addButtonType, ButtonType.CANCEL);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField nom = new TextField();
		nom.setPromptText("Nom");
		ChoiceBox<Magasin> mag = new ChoiceBox<>();
		mag.getItems().addAll(BDD.magasins);
		mag.getSelectionModel().selectFirst();

		grid.add(new Label("Nom:"), 0, 0);
		grid.add(nom, 1, 0);
		grid.add(new Label("Magasin:"), 0, 1);
		grid.add(mag, 1, 1);

		Node addButton = dialog.getDialogPane().lookupButton(addButtonType);

		dialog.getDialogPane().setContent(grid);

		Optional<ButtonType> result = dialog.showAndWait();
		if (!nom.getText().isEmpty() && result.isPresent() && result.get() == addButtonType) {
			Rayon ray = new Rayon(nom.getText(), mag.getSelectionModel().getSelectedItem());
			mag.getSelectionModel().getSelectedItem().addRayon(ray);
			listRayons.getItems().add(ray);
		}
		listRayons.refresh();
	}

	public void onConsultClick() {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Informations sur le rayon");
		alert.setHeaderText(null);
		String nom = "Nom: " + listRayons.getSelectionModel().getSelectedItem().getNom() + "\n";
		String mag = "Magasin: " + listRayons.getSelectionModel().getSelectedItem().getMag() + "\n";

		alert.setContentText(nom + mag);
		alert.showAndWait();
		listRayons.refresh();
	}
}
