package view;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import stock_gestion.*;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

public class PersonnelWindow extends Application {

	@FXML
	private ListView<Personne> listPersonnel;

	@Override
	public void start(final Stage primaryStage) {
		try {
			final URL url = getClass().getResource("/personnelWindow.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(url);
			final AnchorPane root = fxmlLoader.load();
			final Scene scene = new Scene(root, 500, 450);
			primaryStage.setScene(scene);
		} catch (IOException ex) {
			System.err.println("Erreur au chargement: " + ex);
		}
		primaryStage.setTitle("Liste des membres du personnel");

		primaryStage.show();
	}

	public void initialize() {
		listPersonnel.getItems().addAll(BDD.personnel);
	}

	public void onModifyClick() {
		Dialog<ButtonType> dialog = new Dialog<>();
		dialog.setTitle("Modif d'un membre du personnel");
		dialog.setHeaderText(null);


		ButtonType addButtonType = new ButtonType("Modifier", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(addButtonType, ButtonType.CANCEL);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField prenom = new TextField();
		prenom.setPromptText("Prenom");
		prenom.setText(listPersonnel.getSelectionModel().getSelectedItem().getPrenom());
		TextField nom = new TextField();
		nom.setPromptText("Nom");
		nom.setText(listPersonnel.getSelectionModel().getSelectedItem().getNom());
		TextField email = new TextField();
		email.setPromptText("Email");
		email.setText(listPersonnel.getSelectionModel().getSelectedItem().getEmail());
		ChoiceBox<Magasin> mag = new ChoiceBox<>();
		mag.getItems().addAll(BDD.magasins);
		mag.getSelectionModel().selectFirst();

		Label magLab = new Label("Magasin:");
		Label rayLab = new Label("Rayon:");

		ChoiceBox<Rayon> rayon = new ChoiceBox<>();
		nom.setPromptText("Rayon");
		rayon.getItems().addAll(BDD.magasins.get(0).getListeRayons());
		rayon.getSelectionModel().selectFirst();

		mag.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
				rayon.getItems().clear();
				rayon.getItems().addAll(mag.getItems().get(number2.intValue()).getListeRayons());
				rayon.getSelectionModel().selectFirst();
			}
		});

		grid.add(new Label("Prenom:"), 0, 0);
		grid.add(prenom, 1, 0);
		grid.add(new Label("Nom:"), 0, 1);
		grid.add(nom, 1, 1);
		grid.add(new Label("Email:"), 0, 2);
		grid.add(email, 1, 2);
		grid.add(magLab, 0, 3);
		grid.add(mag, 1, 3);
		grid.add(rayLab, 0, 4);
		grid.add(rayon, 1, 4);


		if (listPersonnel.getSelectionModel().getSelectedItem().getClass().getSimpleName().equals("PDG")) {
			mag.setVisible(false);
			rayon.setVisible(false);
			magLab.setVisible(false);
			rayLab.setVisible(false);
		} else if (listPersonnel.getSelectionModel().getSelectedItem().getClass().getSimpleName().equals("ChefDeMagasin")) {
			mag.setVisible(true);
			rayon.setVisible(false);
			magLab.setVisible(true);
			rayLab.setVisible(false);
			mag.getSelectionModel().select(((ChefDeMagasin) listPersonnel.getSelectionModel().getSelectedItem()).getMag());
		} else {
			mag.setVisible(true);
			rayon.setVisible(true);
			magLab.setVisible(true);
			rayLab.setVisible(true);
			mag.getSelectionModel().select(((ChefDeRayon) listPersonnel.getSelectionModel().getSelectedItem()).getMag());
			rayon.getSelectionModel().select(((ChefDeRayon) listPersonnel.getSelectionModel().getSelectedItem()).getRayon());
		}

		Node addButton = dialog.getDialogPane().lookupButton(addButtonType);

		dialog.getDialogPane().setContent(grid);

		Optional<ButtonType> result = dialog.showAndWait();
		if (!prenom.getText().isEmpty() && !nom.getText().isEmpty() && !email.getText().isEmpty() && result.isPresent() && result.get() == addButtonType) {
			if (listPersonnel.getSelectionModel().getSelectedItem().getClass().getSimpleName().equals("PDG")) {

				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(prenom.getText());
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(nom.getText());
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(email.getText());
				listPersonnel.getSelectionModel().getSelectedItem().setPrenom(prenom.getText());
				listPersonnel.getSelectionModel().getSelectedItem().setNom(nom.getText());
				listPersonnel.getSelectionModel().getSelectedItem().setEmail(email.getText());
			} else if (listPersonnel.getSelectionModel().getSelectedItem().getClass().getSimpleName().equals("ChefDeMagasin")) {
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(prenom.getText());
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(nom.getText());
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(email.getText());
				((ChefDeMagasin) BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()))
						.setMag(mag.getSelectionModel().getSelectedItem());
				listPersonnel.getSelectionModel().getSelectedItem().setPrenom(prenom.getText());
				listPersonnel.getSelectionModel().getSelectedItem().setNom(nom.getText());
				listPersonnel.getSelectionModel().getSelectedItem().setEmail(email.getText());
				((ChefDeMagasin) listPersonnel.getSelectionModel().getSelectedItem())
						.setMag(mag.getSelectionModel().getSelectedItem());
			} else {
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(prenom.getText());
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(nom.getText());
				BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()).setNom(email.getText());
				((ChefDeRayon) BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()))
						.setMag(mag.getSelectionModel().getSelectedItem());
				((ChefDeRayon) BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()))
						.setRayon(rayon.getSelectionModel().getSelectedItem());
				listPersonnel.getSelectionModel().getSelectedItem().setPrenom(prenom.getText());
				listPersonnel.getSelectionModel().getSelectedItem().setNom(nom.getText());
				listPersonnel.getSelectionModel().getSelectedItem().setEmail(email.getText());
				((ChefDeRayon) listPersonnel.getSelectionModel().getSelectedItem())
						.setMag(mag.getSelectionModel().getSelectedItem());
				((ChefDeRayon) BDD.personnel.get(listPersonnel.getSelectionModel().getSelectedIndex()))
						.setRayon(rayon.getSelectionModel().getSelectedItem());
			}
		}
		listPersonnel.refresh();
	}

	public void onDeleteClick() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Confirmation de la suppression");
		alert.setContentText("Voulez vous vraiment supprimer l'objet selectionne?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			BDD.personnel.remove(listPersonnel.getSelectionModel().getSelectedItem());
			listPersonnel.getItems().remove(listPersonnel.getSelectionModel().getSelectedIndex());
		}
		listPersonnel.refresh();

	}

	public void onAddClick() {
		Dialog<ButtonType> dialog = new Dialog<>();
		dialog.setTitle("Ajout d'un membre du personnel");
		dialog.setHeaderText(null);


		ButtonType addButtonType = new ButtonType("Ajouter", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(addButtonType, ButtonType.CANCEL);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField prenom = new TextField();
		prenom.setPromptText("Prenom");
		TextField nom = new TextField();
		nom.setPromptText("Nom");
		TextField email = new TextField();
		email.setPromptText("Email");
		ChoiceBox<Magasin> mag = new ChoiceBox<>();
		mag.getItems().addAll(BDD.magasins);
		mag.getSelectionModel().selectFirst();

		Label magLab = new Label("Magasin:");
		Label rayLab = new Label("Rayon:");

		ChoiceBox<Rayon> rayon = new ChoiceBox<>();
		nom.setPromptText("Rayon");
		rayon.getItems().addAll(BDD.magasins.get(0).getListeRayons());
		rayon.getSelectionModel().selectFirst();

		ChoiceBox<String> choiceBox = new ChoiceBox<>();
		choiceBox.getItems().add("PDG");
		choiceBox.getItems().add("Chef de Magasin");
		choiceBox.getItems().add("Chef de Rayon");
		choiceBox.getSelectionModel().selectFirst();

		mag.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
				rayon.getItems().clear();
				rayon.getItems().addAll(mag.getItems().get(number2.intValue()).getListeRayons());
				rayon.getSelectionModel().selectFirst();
			}
		});
		choiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {

				if (observableValue.getValue().toString().equals("0")) {
					mag.setVisible(false);
					rayon.setVisible(false);
					magLab.setVisible(false);
					rayLab.setVisible(false);
				} else if (observableValue.getValue().toString().equals("1")) {
					mag.setVisible(true);
					rayon.setVisible(false);
					magLab.setVisible(true);
					rayLab.setVisible(false);
				} else {
					mag.setVisible(true);
					rayon.setVisible(true);
					magLab.setVisible(true);
					rayLab.setVisible(true);
				}
			}
		});

		grid.add(new Label("Type:"), 0, 0);
		grid.add(choiceBox, 1, 0);
		grid.add(new Label("Prenom:"), 0, 1);
		grid.add(prenom, 1, 1);
		grid.add(new Label("Nom:"), 0, 2);
		grid.add(nom, 1, 2);
		grid.add(new Label("Email:"), 0, 3);
		grid.add(email, 1, 3);
		grid.add(magLab, 0, 4);
		grid.add(mag, 1, 4);
		grid.add(rayLab, 0, 5);
		grid.add(rayon, 1, 5);

		mag.setVisible(false);
		rayon.setVisible(false);
		magLab.setVisible(false);
		rayLab.setVisible(false);


		Node addButton = dialog.getDialogPane().lookupButton(addButtonType);

		dialog.getDialogPane().setContent(grid);

		Optional<ButtonType> result = dialog.showAndWait();
		if (!prenom.getText().isEmpty() && !nom.getText().isEmpty() && !email.getText().isEmpty() && result.isPresent() && result.get() == addButtonType) {
			if (choiceBox.getSelectionModel().getSelectedItem().equals("PDG")) {
				PDG pdg = new PDG(nom.getText(), prenom.getText(), email.getText());
				BDD.personnel.add(pdg);
				listPersonnel.getItems().add(pdg);
			} else if (choiceBox.getSelectionModel().getSelectedItem().equals("Chef de Magasin")) {
				ChefDeMagasin cdm = new ChefDeMagasin(nom.getText(), prenom.getText(), email.getText(),
						mag.getSelectionModel().getSelectedItem());
				BDD.personnel.add(cdm);
				listPersonnel.getItems().add(cdm);
			} else {
				ChefDeRayon cdr = new ChefDeRayon(nom.getText(), prenom.getText(), email.getText(),
						mag.getSelectionModel().getSelectedItem(), rayon.getSelectionModel().getSelectedItem());
				BDD.personnel.add(cdr);
				listPersonnel.getItems().add(cdr);
			}
		}
		listPersonnel.refresh();
	}

	public void onConsultClick() {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Informations sur le membre du personnel");
		alert.setHeaderText(null);
		String nom = "Nom: " + listPersonnel.getSelectionModel().getSelectedItem().getNom() + "\n";
		String prenom = "Prenom: " + listPersonnel.getSelectionModel().getSelectedItem().getPrenom() + "\n";
		String mail = "Email: " + listPersonnel.getSelectionModel().getSelectedItem().getEmail() + "\n";
		String type = "Type: " + listPersonnel.getSelectionModel().getSelectedItem().getClass().getSimpleName() + "\n";
		String rayon = "";
		String mag = "";
		if (!type.contains("PDG")) {
			if (!type.contains("Magasin")) {
				mag = "Magasin: " + ((ChefDeRayon) listPersonnel.getSelectionModel().getSelectedItem()).getMag() + "\n";
				rayon = "Rayon: " + ((ChefDeRayon) listPersonnel.getSelectionModel().getSelectedItem()).getRayon() + "\n";
			} else
				mag = "Magasin: " + ((ChefDeMagasin) listPersonnel.getSelectionModel().getSelectedItem()).getMag() + "\n";
		}

		alert.setContentText(type + prenom + nom + mail + mag + rayon);
		alert.showAndWait();
		listPersonnel.refresh();
	}
}
