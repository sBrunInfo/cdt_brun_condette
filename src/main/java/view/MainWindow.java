package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import stock_gestion.BDD;

import java.io.IOException;
import java.net.URL;


public class MainWindow extends Application {

	@Override
	public void start(final Stage primaryStage) {
		try {
			final URL url = getClass().getResource("/mainWindow.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(url);
			final AnchorPane root = fxmlLoader.load();
			final Scene scene = new Scene(root, 300, 120);
			primaryStage.setScene(scene);
		} catch (IOException ex) {
			System.err.println("Erreur au chargement: " + ex);
		}
		primaryStage.setTitle("Gestion de stock");
		primaryStage.show();
	}

	public void initialize() {
		BDD.instantiateLists();
	}

	public void onConsultMagsClick() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/magasinsWindow.fxml"));
			Parent root1 = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Liste des Magasins");
			stage.setScene(new Scene(root1));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onConsultPersClick() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/personnelWindow.fxml"));
			Parent root1 = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Liste du Personnel");
			stage.setScene(new Scene(root1));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onConsultRayClick() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/rayonsWindow.fxml"));
			Parent root1 = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Liste des Rayons");
			stage.setScene(new Scene(root1));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void onConsultArtClick() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/articlesWindow.fxml"));
			Parent root1 = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Liste des Articles");
			stage.setScene(new Scene(root1));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}