package view;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class LoginWindow extends Application {

	@FXML
	private PasswordField pwField;

	@FXML
	private TextField logField;

	public static void begin() {
		launch();
	}

	@Override
	public void start(final Stage primaryStage) {
		try {
			final URL url = getClass().getResource("/loginWindow.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(url);
			final AnchorPane root = fxmlLoader.load();
			final Scene scene = new Scene(root, 330, 150);
			primaryStage.setScene(scene);
		} catch (IOException ex) {
			System.err.println("Erreur au chargement: " + ex);
		}
		primaryStage.setTitle("Login Gestion de stock");
		primaryStage.show();
	}

	public void checkLogin() {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				this.getClass().getClassLoader().getResourceAsStream("logins.txt")))
		) {
			String login;
			String password;
			boolean found = false;
			while ((login = br.readLine()) != null) {
				password = br.readLine();
				if (logField.getText().equals(login.trim()) && pwField.getText().equals(password.trim())) {
					found = true;
					try {
						FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/mainWindow.fxml"));
						Parent root1 = fxmlLoader.load();
						Stage stage = new Stage();
						stage.setTitle("Gestion de stock");
						stage.setScene(new Scene(root1));
						stage.show();
					} catch (IOException e) {
						e.printStackTrace();
					}
					Stage currStage = (Stage) logField.getScene().getWindow();
					currStage.close();
				}
			}
			if (!found) {
				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Authentification impossible");
				alert.setHeaderText(null);
				alert.setContentText("Le login et/ou mot de passe est incorrect");

				alert.showAndWait();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
