package view;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import stock_gestion.Article;
import stock_gestion.BDD;
import stock_gestion.Magasin;
import stock_gestion.Rayon;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.regex.Pattern;

public class ArticlesWindow extends Application {

	@FXML
	private ListView<Article> listArticles;
	private Pattern isNum = Pattern.compile("-?\\d+(\\.\\d+)?");
	private Pattern isInt = Pattern.compile("[0-9]+");

	@Override
	public void start(final Stage primaryStage) {
		try {
			final URL url = getClass().getResource("/articlesWindow.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(url);
			final AnchorPane root = fxmlLoader.load();
			final Scene scene = new Scene(root, 500, 450);
			primaryStage.setScene(scene);
		} catch (IOException ex) {
			System.err.println("Erreur au chargement: " + ex);
		}
		primaryStage.setTitle("Liste des articles");

		primaryStage.show();
	}

	public void initialize() {
		for (Magasin mag : BDD.magasins) {
			for (Rayon ray : mag.getListeRayons()) {
				listArticles.getItems().addAll(ray.getArticles());
			}
		}
	}


	public boolean isNumeric(String strNum, Pattern pattern) {
		if (strNum == null) {
			return false;
		}
		return pattern.matcher(strNum).matches();
	}

	public void onModifyClick() {
		Dialog<ButtonType> dialog = new Dialog<>();
		dialog.setTitle("Modif d'un article");
		dialog.setHeaderText(null);

		ButtonType addButtonType = new ButtonType("Modifier", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(addButtonType, ButtonType.CANCEL);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField nom = new TextField();
		nom.setPromptText("Nom");
		nom.setText(listArticles.getSelectionModel().getSelectedItem().getNom());
		TextField ref = new TextField();
		ref.setPromptText("Reference");
		ref.setText(listArticles.getSelectionModel().getSelectedItem().getRef());
		TextField quantite = new TextField();
		quantite.setPromptText("Quantite");
		quantite.setText(Integer.toString(listArticles.getSelectionModel().getSelectedItem().getQuantite()));
		TextField prix = new TextField();
		prix.setPromptText("Prix Unitaire");
		prix.setText(Double.toString(listArticles.getSelectionModel().getSelectedItem().getPrixUnitaire()));

		grid.add(new Label("Nom:"), 0, 0);
		grid.add(nom, 1, 0);
		grid.add(new Label("Reference:"), 0, 1);
		grid.add(ref, 1, 1);
		grid.add(new Label("Quantite:"), 0, 2);
		grid.add(quantite, 1, 2);
		grid.add(new Label("Prix unitaire:"), 0, 3);
		grid.add(prix, 1, 3);

		Node addButton = dialog.getDialogPane().lookupButton(addButtonType);

		dialog.getDialogPane().setContent(grid);

		Optional<ButtonType> result = dialog.showAndWait();
		if (!nom.getText().isEmpty() && !ref.getText().isEmpty() &&
				!quantite.getText().isEmpty() && !prix.getText().isEmpty() &&
				isNumeric(quantite.getText(), isInt) && isNumeric(prix.getText(), isNum)
				&& result.isPresent() && result.get() == addButtonType) {
			for (Magasin mag : BDD.magasins) {
				for (Rayon ray : mag.getListeRayons()) {
					for (Article art : ray.getArticles()) {
						if (art.getNom().equals(nom.getText()) && art.getRef().equals(ref.getText())
								&& art.getQuantite() == Integer.parseInt(quantite.getText()) &&
								art.getPrixUnitaire() == Double.parseDouble(prix.getText())) {
							art.setNom(nom.getText());
							art.setRef(ref.getText());
							art.setQuantite(Integer.parseInt(quantite.getText()));
							art.setPrixUnitaire(Double.parseDouble(prix.getText()));
						}
					}
				}
			}
			listArticles.getSelectionModel().getSelectedItem().setNom(nom.getText());
			listArticles.getSelectionModel().getSelectedItem().setRef(ref.getText());
			listArticles.getSelectionModel().getSelectedItem().setQuantite(Integer.parseInt(quantite.getText()));
			listArticles.getSelectionModel().getSelectedItem().setPrixUnitaire(Double.parseDouble(prix.getText()));
		}
		listArticles.refresh();
	}

	public void onDeleteClick() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Confirmation de la suppression");
		alert.setContentText("Voulez vous vraiment supprimer l'objet selectionne?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			for (Magasin mag : BDD.magasins) {
				for (Rayon ray : mag.getListeRayons()) {
					ray.getArticles().removeIf(art -> art.equals(listArticles.getSelectionModel().getSelectedItem()));
				}
			}
			listArticles.getItems().remove(listArticles.getSelectionModel().getSelectedIndex());
		}
		listArticles.refresh();

	}

	public void onAddClick() {
		Dialog<ButtonType> dialog = new Dialog<>();
		dialog.setTitle("Ajout d'un article");
		dialog.setHeaderText(null);


		ButtonType addButtonType = new ButtonType("Ajouter", ButtonBar.ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(addButtonType, ButtonType.CANCEL);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField nom = new TextField();
		nom.setPromptText("Nom");
		TextField ref = new TextField();
		ref.setPromptText("Reference");
		TextField quantite = new TextField();
		quantite.setPromptText("Quantite");
		TextField prix = new TextField();
		prix.setPromptText("Prix Unitaire");

		ChoiceBox<Magasin> mag = new ChoiceBox<>();
		mag.getItems().addAll(BDD.magasins);
		mag.getSelectionModel().selectFirst();

		ChoiceBox<Rayon> rayon = new ChoiceBox<>();
		rayon.getItems().addAll(BDD.magasins.get(0).getListeRayons());
		rayon.getSelectionModel().selectFirst();

		mag.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
				rayon.getItems().clear();
				rayon.getItems().addAll(mag.getItems().get(number2.intValue()).getListeRayons());
				rayon.getSelectionModel().selectFirst();
			}
		});

		grid.add(new Label("Nom:"), 0, 0);
		grid.add(nom, 1, 0);
		grid.add(new Label("Reference:"), 0, 1);
		grid.add(ref, 1, 1);
		grid.add(new Label("Quantite:"), 0, 2);
		grid.add(quantite, 1, 2);
		grid.add(new Label("Prix unitaire:"), 0, 3);
		grid.add(prix, 1, 3);
		grid.add(new Label("Magasin:"), 0, 4);
		grid.add(mag, 1, 4);
		grid.add(new Label("Rayon:"), 0, 5);
		grid.add(rayon, 1, 5);

		Node addButton = dialog.getDialogPane().lookupButton(addButtonType);

		dialog.getDialogPane().setContent(grid);

		Optional<ButtonType> result = dialog.showAndWait();
		if (!nom.getText().isEmpty() && !ref.getText().isEmpty() &&
				!quantite.getText().isEmpty() && !prix.getText().isEmpty() &&
				isNumeric(quantite.getText(), isInt) && isNumeric(prix.getText(), isNum)
				&& result.isPresent() && result.get() == addButtonType) {
			Article art = new Article(nom.getText(), ref.getText(), Integer.parseInt(quantite.getText()),
					Double.parseDouble(prix.getText()));
			rayon.getSelectionModel().getSelectedItem().getArticles().add(art);
			listArticles.getItems().add(art);
		}
		listArticles.refresh();
	}

	public void onConsultClick() {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Informations sur l'article'");
		alert.setHeaderText(null);
		String nom = "Nom: " + listArticles.getSelectionModel().getSelectedItem().getNom() + "\n";
		String ref = "Reference: " + listArticles.getSelectionModel().getSelectedItem().getRef() + "\n";
		String quant = "Quantite: " + listArticles.getSelectionModel().getSelectedItem().getQuantite() + "\n";
		String prix = "Prix unitaire: " + listArticles.getSelectionModel().getSelectedItem().getPrixUnitaire() + "€\n";

		alert.setContentText(nom + ref + quant + prix);
		alert.showAndWait();
		listArticles.refresh();
	}
}
