package view;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import stock_gestion.BDD;
import stock_gestion.Magasin;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

public class MagasinsWindow extends Application {

	@FXML
	private ListView<Magasin> listMagasins;

	@Override
	public void start(final Stage primaryStage) {
		try {
			final URL url = getClass().getResource("/magasinsWindow.fxml");
			final FXMLLoader fxmlLoader = new FXMLLoader(url);
			final AnchorPane root = fxmlLoader.load();
			final Scene scene = new Scene(root, 500, 450);
			primaryStage.setScene(scene);
		} catch (IOException ex) {
			System.err.println("Erreur au chargement: " + ex);
		}
		primaryStage.setTitle("Liste des magasins");

		primaryStage.show();
	}

	public void initialize() {
		listMagasins.getItems().addAll(BDD.magasins);
	}

	public void onModifyClick() {
		TextInputDialog dialog = new TextInputDialog(listMagasins.getSelectionModel().getSelectedItem().getNom());
		dialog.setTitle("Modification d'un Magasin");
		dialog.setHeaderText(null);
		dialog.setContentText("Nom:");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			BDD.magasins.get(listMagasins.getSelectionModel().getSelectedIndex()).setNom(result.get());
			listMagasins.getSelectionModel().getSelectedItem().setNom(result.get());
		}
		listMagasins.refresh();
	}

	public void onDeleteClick() {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Confirmation de la suppression");
		alert.setContentText("Voulez vous vraiment supprimer l'objet selectionne?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			BDD.magasins.remove(listMagasins.getSelectionModel().getSelectedItem());
			listMagasins.getItems().remove(listMagasins.getSelectionModel().getSelectedIndex());
		}
		listMagasins.refresh();

	}

	public void onAddClick() {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Ajout d'un Magasin");
		dialog.setHeaderText(null);
		dialog.setContentText("Nom:");

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			BDD.magasins.add(new Magasin(result.get()));
			listMagasins.getItems().add(new Magasin(result.get()));
		}
		listMagasins.refresh();
	}

	public void onConsultClick() {
		Alert alert = new Alert(Alert.AlertType.INFORMATION);
		alert.setTitle("Informations sur le magasin");
		alert.setHeaderText(null);
		String name = listMagasins.getSelectionModel().getSelectedItem().getNom() + "\n";
		alert.setContentText("Nom: " + name);

		alert.showAndWait();
		listMagasins.refresh();
	}
}
