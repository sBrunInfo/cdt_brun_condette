# Fonctionnalités
1.	Gestion des stocks (CRUD articles, 1 magasin et qq rayons pré-créés)
2.	Rôles (droits), authentification (avec users pré-créés)
3.	Reste CRUD magasins, rayons
4.	Reste CRUD users

# Livrables
- specs
- toutes les sources
- cahier de recettes (tests manuels)
- rapports de tests auto (JUnit surtout, mais aussi Sonar, Selenium ou équivalent)
- rapport de blabla pour justifier qu'on a fait des démarches qualité (genre TDD, GitLab CI)

# Contacts
- francois.senis@equensworldline.com
- otmane.elguenouni@equensworldline.com

# Notes

## Relations
- 1 PDG
    - Plusieurs magasins (1 chef par magasin)
        - Plusieurs rayons (1 chef par rayon)
            - Plusieurs articles

## Droits
- User lambda (e.g. un client) : read magasins / rayons / articles
- Chef de rayon : + CRUD articles dans son rayon
- Chef de magasin : + CRUD tous rayons ET users en-dessous
- PDG : ALL

## Données

- (Tous) Users : nom prénom mail
- Articles : nom ref qté prixUnit
- Rayons / Magasins : nom + relations (chef, articles, ...)

## Conseils

- Pas besoin de faire un truc sur internet
- Pas besoin de faire une BDD évoluée (un fichier à la con suffit)